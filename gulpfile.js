// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint 	= require('gulp-jshint');
var sass 	= require('gulp-sass');
var gulpconcat 	= require('gulp-concat');
var uglify 	= require('gulp-uglify');
var rename 	= require('gulp-rename');
var livereload 	= require('gulp-livereload');
var mainBowerFiles = require('main-bower-files');

livereload({ start: true });

// Lint Task
gulp.task('lint', function() {
    return gulp.src('assets/script/src/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('applicationsass', function() {
    return gulp.src('assets/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('assets/css'));
});

// Concatenate & Minify JS
gulp.task('applicationscripts', function() {
    return gulp.src('assets/script/src/**/*.js')
        .pipe(gulpconcat('application.js'))
        .pipe(gulp.dest('assets/script/bin'))
        .pipe(rename('application.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/script/bin'));
});

// Concatenate & Minify JS
gulp.task('componentscripts', function() {
    return gulp.src('assets/lib/**/*.js')
        .pipe(gulpconcat('components.js'))
        .pipe(gulp.dest('assets/script/bin'))
        .pipe(rename('components.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/script/bin'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('bower_components/**', ['bower']);
    gulp.watch('assets/script/src/**/*.js', ['lint', 'applicationscripts']);
    gulp.watch('assets/script/lib/**/*.js', ['lint', 'componentscripts']);
    gulp.watch('assets/scss/**/*.scss', ['applicationsass']);
});

gulp.task('bower', function() {
    // mainBowerFiles is used as a src for the task,
    // usually you pipe stuff through a task
    return gulp.src(mainBowerFiles())
        .pipe(gulp.dest('assets/lib'))
});

// Default Task
gulp.task('default', ['lint', 'applicationsass', 'applicationscripts', 'bower', 'componentscripts']);
